
package com.examples.web;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 *
 * @author rodrigo.nascimento
 */
public class BemVindo extends HttpServlet {


    /**
     * O método doPost() proveniente da classe extendida HttpServelt fará uma chamada ao metodo processRequest.
     * O metodo processRequest é uma implementação da Classe BemVindo.java, o metodo poderia ter qualquer outro nome.
     * O servlet suporta chamadas HTTP do tipo Post, o metodo doPost() delega a referencia de request e response.
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        /**
         * Define a resposta como sendo um HTML, note que response é a referencia recebida de doPost().
         */
        response.setContentType("text/html;charset=UTF-8");
        
        /**
         * Captura uma instancia de PrintWrite em response para escrever na resposta HTML.
         */
        PrintWriter out = response.getWriter();
        
        try {
            /**
             * Escreve na resposta HTML.
             */
            
            /**
             * O container ira converter o objeto HttpServletResponse em um Response HTTP.
             * O Response HTTP contem uma pagina HTML que será entregue ao bowser imediatamente após a execução deste metodo.
             */   
            
            //Na estrutura padrão do HTML que será retornado, estamos incluindo o texto 'Seja bem vindo'.
            out.println("Seja Bem vindo!");  

        } finally { 
            /**
             * Fecha o Stream de escrita.
             */
            out.close();
        }
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Delega ao metodo processRequest desta classe todos os privilégios do metodo doPost.
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
