
package com.examples.web;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author rodrigo.nascimento
 */
public class Calcular extends HttpServlet {
   
     /**
     * O método doPost() proveniente da classe extendida HttpServelt fará uma chamada ao metodo processRequest.
     * O metodo processRequest é uma implementação da Classe Calcular.java, o metodo poderia ter qualquer outro nome.
     * O servlet suporta chamadas HTTP do tipo Post, o metodo doPost() está delegando a referencia de request e response.
     */
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {

        /*
         * Captura uma instancia de PrintWrite em response para escrever na resposta HTML.
         */
        PrintWriter out = response.getWriter();
        
        try {
            
           /**
            * Define a resposta como sendo um HTML, note que response é a referencia recebida de doPost().
            */
            response.setContentType("text/html;charset=UTF-8");
            

            /**
             * Capitura os parametros do Objeto request.
             * 
             * O container vê q a solicitação do cliente é para um servlet e cria dois Objetos.
             * Os objtos são respectivamente o HttpServletRequest e HttpServletResponse.
             * O container cria um par de String nome/valor e passa como parametro para os objetos request e response.
             */
            
            //Converte o valores capturados do formulário que fez a chamada ao metodo doPost() em inteiros.
            Integer n1 = Integer.parseInt(request.getParameter("nota1"));
            Integer n2 = Integer.parseInt(request.getParameter("nota2"));
        
           /**
            * O container ira converter o objeto HttpServletResponse em um Response HTTP.
            * O Response HTTP contem uma pagina HTML que será entregue ao bowser imediatamente após a execução deste metodo.
            */   
        
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Calcular</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Calcular at " + request.getContextPath () + "</h1>");
            out.println("<br> Nota 1: " + n1 + "<br> Nota 2: "+ n2);
            out.println("<br>Média: " + (n1 + n2) /2);
            
            /**
             * Os parametros iniciais definidos no Deployment Descripitor (***web.xml***) são capturados através
             * do metodo getServletConfig implementado na Classe GenericServelet extendida por HttpServlet.
             * 
             * Lembre-se, os parametros estão disponiveis apenas para este Servlet (Classe).
             */
            out.println("<br><br><br><br>Parametros init do ServletConfig");
            out.println("<br><br>Residencial: " + getServletConfig().getInitParameter("residencial"));
            out.println("<br>Celular: " + getServletConfig().getInitParameter("cel"));
            out.println("</body>");
            out.println("</html>");

        } finally { 
            /**
             * Fecha o Stream de escrita.
             */
            out.close();
        }
    } 


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {

    } 

    /**
     * Delega a processRequest todos os privilégios de doPost.
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
