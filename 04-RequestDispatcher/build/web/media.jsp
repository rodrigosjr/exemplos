
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Média Escolar</title>
    </head>
    <body>
        <h1>Resultado</h1>
        
        <!-- JSP são transformados em servlets de primeira classe e recebem seus próprios ServletConfig. -->
        
        <% 
            /**
             * Capitura os parametros do Objeto request.
             * 
             * O objeto request e response foi recebido pelo servlet Calcular.java e posteriormente encaminhado a JSP
             * vide implementação do metodo processRequest().
             * 
             * O container vê q a solicitação do cliente é para um servlet e cria dois Objetos, HttpServletRequest e 
             * HttpServletResponse respectivamente.
             */
        
            /**
             * O container cria um par de String nome/valor e passa como parametro para os objetos request e response.
             * O valor pode ser recuperado ao informar o nome para o metodo getParameter() da Classe ServeletRequest.java
             * extendida por HttpServletRequest.java.
             */
            Integer n1 = Integer.parseInt(request.getParameter("nota1"));
            Integer n2 = Integer.parseInt(request.getParameter("nota2"));
            
            /**
             * O JSP ira gerar um Response HTTP.
             * O Response HTTP contem uma pagina HTML que será entregue ao bowser.
             */ 
            
           //out é uma instancia implicita de java.​io.​Writer obtida do objeto response.
            out.println("<h1>Servlet Calcular at " + request.getContextPath () + "</h1>");
            out.println("<br> Nota 1: " + n1 + "<br> Nota 2: "+ n2);
            out.println("<br>Média: " + (n1 + n2) /2);
            
        //Ao final da execução do Servelet, o processo retorna para o metodo processRequest().
        %>
    </body>
</html>
