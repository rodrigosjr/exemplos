
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Média Escolar</title>
    </head>
    <body>
        <h1>Resultado</h1>
        
        <!-- 
         * JSP são transformados em servlets de primeira classe e recebem seus próprios ServletConfig. 
         * 
         * O Objeto ServletConfig de outros Servlets podem ser passados para este por meio de parametros,
         * vide implementação do metodo processRequest().
        -->
        
        <% 
           /**
             * Capitura os parametros do Objeto request.
             * 
             * O objeto request e response foi recebido pelo servlet Calcular.java e posteriormente encaminhado a JSP
             * vide implementação do metodo processRequest().
             * 
             * O container vê q a solicitação do cliente é para um servlet e cria dois Objetos, HttpServletRequest e 
             * HttpServletResponse respectivamente.
             */
        
            /**
             * O container cria um par de String nome/valor e passa como parametro para os objetos request e response.
             * O valor pode ser recuperado ao informar o nome para o metodo getParameter() da Classe ServeletRequest.java
             * extendida por HttpServletRequest.java.
             */
        
            Integer n1 = Integer.parseInt(request.getParameter("nota1"));
            Integer n2 = Integer.parseInt(request.getParameter("nota2"));
            
            /**
             * Capitura um Objeto ServletConfig atribuido à request na implementação do metodo processRequest().
             * 
             * O Objeto ServletConfig pertence ao Servlet 'Calcular.java' declarado no Deployment Descripitor (***web.xml***).
             */
            ServletConfig sc = (ServletConfig) request.getAttribute("ServletConfig");
            
            /**
             * O JSP ira gerar um Response HTTP.
             * O Response HTTP contem uma pagina HTML que será entregue ao bowser.
             */ 
                       
             //out é uma instancia implicita de java.​io.​Writer obtida do objeto response.
            out.println("<h1>Servlet Calcular at " + request.getContextPath () + "</h1>");
            out.println("<br> Nota 1: " + n1 + "<br> Nota 2: "+ n2);
            out.println("<br>Média: " + (n1 + n2) /2);
            out.println("<br><br><br><br>Parametros init do ServletConfig");
            out.println("<br><br>Residencial: " + sc.getInitParameter("residencial"));
            out.println("<br>Celular: " + sc.getInitParameter("cel"));
        %>
    </body>
</html>
