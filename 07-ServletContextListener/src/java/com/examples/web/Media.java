/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.examples.web;

/**
 *
 * @author rodrigo.nascimento
 */
public class Media {
    
    private String nomeMateria;

    
    public Media(String nomeMateria){
        this.nomeMateria=nomeMateria;
    }
    
    public String getNomeMateria() {
        return nomeMateria;
    }  
    
    public float calcular(int nota1, int nota2){
        return (nota1 + nota2) / 2;
    }
    
}
