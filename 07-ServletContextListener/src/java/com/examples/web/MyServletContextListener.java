/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.examples.web;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 *
 * @author rodrigo.nascimento
 */

//A interface ServletContextListener Notifica qdo o contexto é inicializado e encerrado.
public class MyServletContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        /**
         * Solicita o servletContext ao evento.
         */
        ServletContext sc = sce.getServletContext();
        
        /**
         * Utiliza o contexto para obter o parametro init.
         */
        String nomeMateria = sc.getInitParameter("materia");
        
        /**
         * Cria um novo objeto Media
         */
        Media m = new Media(nomeMateria);
        
        /**
         * Utiliza o contexto para setar um atributo passando como parametro o nome de referencia e o valor/objeto.
         * 
         * Outros trechos da aplicação serão capazes de recuperar o objeto incluso no contexto, uma vez que está disponivel para toda a aplicação.
         */
        sc.setAttribute("media", m);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
