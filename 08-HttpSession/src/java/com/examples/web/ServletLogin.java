
package com.examples.web;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;


public class ServletLogin extends HttpServlet {

     /**
     * O método doPost() proveniente da classe extendida HttpServelt fará uma chamada ao metodo processRequest.
     * O metodo processRequest é uma implementação da Classe ServeletLogin.java, o metodo poderia ter qualquer outro nome.
     * O servlet suporta chamadas HTTP do tipo Post, o metodo doPost() está delegando a referencia de request e response.
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            
            /**
             * Este Servlet é chamado pelo Form da pagina index.html através do parametro action.
             * 
             * O container cria um par de String nome/valor e passa como parametro para os objetos request e response.
             * O valor pode ser recuperado ao informar o nome para o metodo getParameter() da Classe ServeletRequest.java
             * extendida por HttpServletRequest.java.
             * 
             * Lembre-se, os privilégios a request e response foram delegados pelo metodo doPost() sobrescrito nessa Classe.
             */

            if ("rodrigo".equals(request.getParameter("nome")) && "1234".equals(request.getParameter("senha"))){
                
                // Captura uma sessão, caso não houvesse uma, seria criada uma nova sessão.
                
                HttpSession s = request.getSession();
                
                /**
                 * Atribui um objeto String a sessão, a sessão esta acessivel apenas a um cliente.
                 * 
                 * A sessão armazena variaveis e objetos para um usuário em particular, as variaveis e objetos 
                 * existirão enquanto a sessão do usuário existir, diferente do request onde as variaveis e objetos
                 * existem apenas enquanto durar a requisição.
                 * 
                 * A sessão também difere do contexto (Visto no exercício anterior), pois o Contexto está disponivel
                 * para a aplicação, ou seja, todos os seus clientes. As variaveis e Objetos existem enquanto a 
                 * aplicação estiver em execução.
                 * 
                 * O nome (String) é encapsulado em um objeto do tipo String.
                 */
                s.setAttribute("nome", request.getParameter("nome"));
           
                //Neste caso não é atribuido a pagina HTML o controle de request e respose.
                response.sendRedirect("media.jsp");
                
            }else{
                
                /**Se a autenticação falhar o usuário será redirecionado a pagina invalido.
                 *
                 * Neste caso não é atribuido a pagina HTML o controle de request e respose.
                 */
                response.sendRedirect("invalido.html");
            }
            

        } finally {            

        }
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    /**
     * Delega a processRequest todos os privilégios de doPost.
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
