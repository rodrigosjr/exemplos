<%-- 
    Document   : media
    Created on : 11/04/2012, 08:45:58
    Author     : rodrigo.nascimento
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  </head>
  <body>
      <h1 align="center">Calcule a média</h1>
      
        <!-- 
         * JSP são transformados em servlets de primeira classe e recebem seus próprios ServletConfig. 
         * 
         * O Objeto ServletConfig de outros Servlets podem ser passados para este por meio de parametros.
        --> 

      <% 
          /**
           * out é uma instancia implicita de java.​io.​Writer obtida do objeto response.
           * 
           * Captura o nome do usuario registrado na sessão.
           */
          out.println("Bem vindo " + request.getSession().getAttribute("nome")); 
      %>
      
      <!-- 
        * Neste exemplo o parametro action direciona a requisição diretamente para uma pagina JSP.
        *
        * Não foi especificada uma classe para tratar a requisição no Deployment Descripitor (***web.xml***)
        * como vinhamos utilizando, inclusive na index.html deste exemplo.
      -->
      <form action="resultadoMedia.jsp" method="post">
          Nota 1: <input name="nota1" type="text" ><br>
          Nota 2: <input name="nota2" type="text" ><br>
          <input type="submit" value="Resposta"/>
      </form>
  </body >
</html>

