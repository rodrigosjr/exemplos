<%-- A diretiva page possui 13 atributos, como o contentType que define o tipo de resposta --%>
<%-- Exemplo de declaração de dois atributos --%>
<%@page contentType="text/html" pageEncoding="UTF-8" session="false" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Média Escolar</title>
    </head>
    <body>
        <h1>Resultado</h1>
        
        <!-- 
         * JSP são transformados em servlets de primeira classe e recebem seus próprios ServletConfig. 
         * 
         * O Objeto ServletConfig de outros Servlets podem ser passados para este por meio de parametros.
        -->
        
        <% 
           /**
             * Capitura os parametros do Objeto request.
             * 
             * O objeto request e response foi recebido diretamente pelo servlet media.jsp, sem o intermedio de uma Classe
             * que extenda HttpServlet.java.
             * 
             * O container vê q a solicitação do cliente é para um servlet e cria dois Objetos, HttpServletRequest e 
             * HttpServletResponse respectivamente.
             */
        
            /**
             * O container cria um par de String nome/valor e passa como parametro para os objetos request e response.
             * O valor pode ser recuperado ao informar o nome para o metodo getParameter() da Classe ServeletRequest.java
             * extendida por HttpServletRequest.java.
             */
        
            Integer n1 = Integer.parseInt(request.getParameter("nota1"));
            Integer n2 = Integer.parseInt(request.getParameter("nota2"));
            
            /**
             * Capitura um Objeto ServletConfig do Servlet gerado a partir da JSP.
             * 
             * config é uma variavel implicita.
             * 
             * Nos exercicios anteriores capturavamos o ServletConfig do atributo de request, conforme abaixo:
             * ServletConfig sc = (ServletConfig) request.getAttribute("ServletConfig");
             */
            ServletConfig sc = config;
            
            /**
             * O JSP ira gerar um Response HTTP.
             * O Response HTTP contem uma pagina HTML que será entregue ao bowser.
             */ 
                       
            //out é uma instancia implicita de java.​io.​Writer obtida do objeto response.
            out.println("<h1>Servlet Calcular at " + request.getContextPath () + "</h1>");
            out.println("<br> Nota 1: " + n1 + "<br> Nota 2: "+ n2);
            out.println("<br>Média: " + (n1 + n2) /2);
            out.println("<br><br><br><br>Parametros init do ServletConfig");
            out.println("<br><br>Residencial: " + sc.getInitParameter("residencial"));
            out.println("<br>Celular: " + sc.getInitParameter("cel"));
        %>
    </body>
</html>
