
package com.examples.web;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author rodrigo.nascimento
 */
public class Calcular extends HttpServlet {
   
     /**
     * O método doPost() proveniente da classe extendida HttpServelt fará uma chamada ao metodo processRequest.
     * O metodo processRequest é uma implementação da Classe Calcular.java, o metodo poderia ter qualquer outro nome.
     * O servlet suporta chamadas HTTP do tipo Post, o metodo doPost() está delegando a referencia de request e response.
     */
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {

        try {
            
            /**
             * Define a resposta como sendo um HTML, note que response é a referencia recebida de doPost().
             */
            
            response.setContentType("text/html;charset=UTF-8");
            
            
            /**
             * Insere na requisição um atributo chamado "media", que possuirá uma referencia ao Objeto Média
             * 
             * A pagina JSP que receber o controle do request e do response deverá possuir um Bean para trabalhar
             * com o atributo "media". 
             */
            
            Media media = new Media();
            media.setNota1(5);
            media.setNota2(3);
            
            request.setAttribute("media", media);
            
            /**
             * O RequestDispatcher tem como objetivo Redirecionar o controle para um outro componente da aplicação.
             * O controle é atribuido a media.jsp para que ele termine de atender a solicitação do cliente.
             * 
             * JSP são transformados em servlets de primeira classe e recebem seus próprios ServletConfig.
             */
            
            RequestDispatcher rd = request.getRequestDispatcher("media.jsp");
            
            /**
             * Atribui o controle do request e response ao JSP.
             */
            
            rd.forward(request, response);
            
            /**
             * Forward pode ser substituido por include.
             * O "include" serve, como o próprio nome diz, para incluir recursos web na sua página atual (Objeto response). 
             * É no reponse que ele age principalmente, e também pode ser chamado quantas vezes você quiser.
         
             * Ex.:

             * RequestDispatcher dispatcher = request.getRequestDispatcher("A.jsp");   
             * dispatcher.include(request, response);
             * dispatcher = request.getRequestDispatcher("B.jsp");   
             * dispatcher.include(request, response);  

             * Isso fará tanto a página A.jsp quanto a B.jsp serem incluídas na sua página de retorno para o usuário (response)!
             * Lembre-se: A ação include não redireciona, apenas inclui, por isso você pode chama-lá e continuar executando código logo 
             * após, porque ainda está no mesmo recurso web - no caso, no teu servlet! 
             */
        } finally { 

        }
    } 


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
   
    } 

    /**
     * Delega a processRequest todos os privilégios de doPost.
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
