/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.examples.web;

/**
 *
 * @author rodrigo.nascimento
 */

//A Classe Media implementa a interface Nota e sobrescreve seus metodos.
public class Media implements Nota {
    private int nota1;
    private int nota2;

    //Os metodos sobrepostos devem conter a anotação @Override
    @Override
    public int getNota1() {
        return nota1;
    }

    @Override
    public void setNota1(int nota1) {
        this.nota1 = nota1;
    }

    @Override
    public int getNota2() {
        return nota2;
    }

    @Override
    public void setNota2(int nota2) {
        this.nota2 = nota2;
    }
    
    public double media(){
        return (nota1 + nota2) / 2;
    }
}
