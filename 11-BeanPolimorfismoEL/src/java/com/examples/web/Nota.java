/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.examples.web;

/**
 *
 * @author rodrigo.nascimento
 */

/**
 * Nota é uma interface que dita como os metodos Getter e Setter devem se parecer.
 * 
 * As Classes que implementarem a interface Nota deverá implementar os seus metodos, sobrescrevendo-os.
 */
public interface Nota {

    public int getNota1();
    
    public void setNota1(int nota1);
    
    public int getNota2();
    
    public void setNota2(int nota2);
}
