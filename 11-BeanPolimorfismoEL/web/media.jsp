
<%@page import="com.examples.web.Media"%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Média Escolar</title>
    </head>
    <body>
        <h1>Resultado</h1>
        
        <!-- 
         * JSP são transformados em servlets de primeira classe e recebem seus próprios ServletConfig. 
         * 
         * O Objeto ServletConfig de outros Servlets podem ser passados para este por meio de parametros.
        -->
        
        <!-- 
         * Declara e inicializa um objeto bean.
         *
         * Beans não possuem suporte a construtores com argumentos, somente construtor padrão e publico,
         * o que irá impedi-lo de instanciar um objeto passando parametros.
         *
         * Os códigos criados a partir dos beans são inseridos dentro do metodo service() da classe extendida HttpServlet.java. 
        -->
        
        <!-- Abre a TAG jsp:useBean -->        
        <jsp:useBean id="media" type="com.examples.web.Nota" class="com.examples.web.Media" scope="request" >
        <!-- 
         * O parametro type está especificando que a Classe Media implementa a interface Nota.
         *
         * Isso que dizer que a referencia do Objeto atribuida ao escopo pela Bean será do Tipo Nota
         * e não do Tipo do Objeto instanciado, este é o conceito de polimorfismo.
        -->
            
        <!--
           @id=identificador (String)
           @type=Tipo (Referencia do objeto. Pode ser omitido.)
           @class=Classe (Representa a referencia (Objeto) e o tipo de instancia que será criada.).
           @scope=Escopo (request, session, context)
        -->
            
        <!-- 
         * No exemplo acima o Bean procura por um atributo chamado media (id="media") no escopo de request (scope="request").
         *
         * Se não puder encontrar um objeto que corresponda ao atributo e ao escopo definido na estrutura, o Bean constroi um. 
         *
         * Considerando que já Criamos uma instancia do Obejto Media na Classe Calcular.java e atribuimos ao escopo 
         * de resquest utilizando a String 'media', a Bean irá encontrar a correspondencia do atributo e escopo 
         * declarado na estrutura e não irá fazer nada.
        -->

       <!--
        * Se o objeto existir no escopo resquest, session ou context, os setters não serão executados, 
        * por estarem dentro da tag '<'jsp:useBean'>'
        *
        * Neste caso os parmetros da requisição enviada pelo formulario da pagina index.html não serão atribuidos
        * ao Objeto Media, fazendo com que o Objeto Media utilize os parametros (Notas) fornecidos na implementação do
        * Metodo processRequest() da Classe Calcular.java.
       -->
       <jsp:setProperty name="media" property="*" />
       
       <%-- 
            Ao mover a tag <jsp:setProperty> para fora da tag </jsp:useBean>, os parametros do Objeto Media 
            serão substituidos pelos parametros da requisição enviada pelo formulario da pagina index.html.
       -->
        
        <!-- 
         * Atribui os parametros de request nas variaveis de instancia do Objeto Media, os parametros recebidos
         * na requisição do formulário da pagina index.html são 'nota1' e 'nota2' respectivamente.
        -->

        <%-- EXEMPLO 1
             <jsp:setProperty name="media" property="nota1" param="nota1" />
             <jsp:setProperty name="media" property="nota2" param="nota2" />
        --%>
        
        <!-- 
           @name     = nome        (nome da referencia do objeto Media)
           @property = propriedade (É o mesmo que media.setNota1(int nota1))     Obs. É preciso ter a mesma nomenclatura do metodo,sem o 'set'.
           @param    = parametro   (É o mesmo que request.getParameter("nota1")) Obs. É precisa ter a mesma nomenclatura q o ID do input do formulario)
        -->   
            
        <%--
             Configurar o id dos inputs (param) dos formularios com a mesma nomenclatura dos metodos, sem o 'set' (property)
             permite a omissão da tag param="" na estrutura de <jsp:setProperty>.

             EXEMPLO 2
             <jsp:setProperty name="media" property="nota1" />
             <jsp:setProperty name="media" property="nota2" />
        --%>
            
        <%-- 
             Pode-se utilizar código java dentro do corpo da bean, porem não é recomendado.
        
             EXEMPLO 3
             <jsp:setProperty name="media" property="nota1" value="<%= request.getParameter("nota1")%>" />
             <jsp:setProperty name="media" property="nota2" value="<%= request.getParameter("nota2")%>" />
        --%>
            
        <!-- 
         * Pode-se utilizar um coringa com a tag property="*" o que discarta o necessidade de criar um setProperty para cada 
         * parametro da instancia.
         *
         * Para tanto o id do input do formuario deve ser nomeado de acordo com a propriedade da instancia, ou seja, seus getters e setters. --%> 
        
        <!-- A tag value="" permite que um valor seja informado diretamente no bean. -->
        
        </jsp:useBean>
        <!-- Fecha a TAG jsp:useBean -->       
        
        <!-- 
         * Capitura o estado do atributo
         *
         * @name     = nome        (nome da referencia ao objeto Media)
         * property = propriedade (É o mesmo que media.getNota1() Obs. É preciso ter a mesma nomenclatura do metodo, sem o 'set'.
        -->

        Nota 1: <jsp:getProperty name="media" property="nota1" /><br> 
        Nota 2: <jsp:getProperty name="media" property="nota2" /><br>
        
        <!-- 
         * Expression Languages, ou simplesmente EL.
         *
         * O EL surgiu para substituir os scriptlets que vinhamos utilizando nos exemplos anteriores 
         * dentro das paginas JSP entre as TAGs '<'%...%'>'. 
         *
         * O EL permite que possamos referenciar as Beans e acessar os metodos dos objetos. 
        -->
        
        Sua média é: ${media.media()}
        
        <%--
            Sem o EL teriamos que acessar o metodo do Objeto conforme exemplo abaixo:
        
            <%
                Media m = (Media) request.getAttribute("media");
                out.println("<br>Sua média é: " + m.media());
            %>
        --%>
 
    </body>
</html>
