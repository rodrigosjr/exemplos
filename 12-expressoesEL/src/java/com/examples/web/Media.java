/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.examples.web;

/**
 *
 * @author rodrigo.nascimento
 */
public class Media {

    private int nota1;
    private int nota2;

    public int getNota1() {
        return nota1;
    }

    public void setNota1(int nota1) {
        this.nota1 = nota1;
    }

    public int getNota2() {
        return nota2;
    }

    public void setNota2(int nota2) {
        this.nota2 = nota2;
    }
    
    public double media(int divisor){
        return (nota1 + nota2) / divisor;
    }  
}
