
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Média Escolar</title>
    </head>
    <body>
        <h1>Resultado</h1>
        
        <!-- 
         * JSP são transformados em servlets de primeira classe e recebem seus próprios ServletConfig. 
         * 
         * O Objeto ServletConfig de outros Servlets podem ser passados para este por meio de parametros.
        -->
        
        <!-- 
         * Expression Languages, ou simplesmente EL.
         *
         * O EL surgiu para substituir os scriptlets que vinhamos utilizando nos exemplos anteriores 
         * dentro das paginas JSP entre as TAGs '<'%...%'>'. 
         *
         * O EL permite que possamos referenciar as Beans e acessar os metodos dos objetos. 
         *
         * Através do EL também podemos acessar o Objeto implicito 'param' para acessar os atributos de request.
        -->
        
        <!-- 
         * Os objetos implicitos são simples Maps para nome/valor 
         *
         * após o operador ponto deve se utilizar o ID utilizado no input do formulario que fez a requisição.
        -->
        Nome: ${param.nome} <br> 
        Sobrenome: ${param.sobreNome} <br>
        
        <!-- 
         * Se o formulario que fez a requisição possuir IDs identicos os valores serão inclusos em um ArrayList.
         *
         * ParamValue permite acessar os diferentes valores para o parametro.
         *
         * Considere que o formulario da pagina index.html possui dois inputs com o mesmo nome, 'nota'.
        -->
        Nota 1: ${paramValues.nota[0]}<br>
        Nota 2: ${paramValues.nota[1]}<br>
        
        
        <!-- Tenta capturar um atributo com o nome media no escopo de request, como não existe, um objeto será criado. -->
        <jsp:useBean id="media" class="com.examples.web.Media" scope="request" >
            <!-- 
             * Considerando que o objeto não existe no escopo, os SETTERS serão iniciados 
             * e receberão como parametros para os atributos os parametros EL 
            -->
            <jsp:setProperty name="media" property="nota1" value="${paramValues.nota[0]}"/>
            <jsp:setProperty name="media" property="nota2" value="${paramValues.nota[1]}"/>
        </jsp:useBean>
            
        <!-- 
         * O Objeto media é uma bean, portanto pode ser acessado através do EL 
         * 
         * EL permite que possamos passar parametros para os métodos.
        -->

        Sua média é: ${media.media(2)}
        
        <br>
        <!-- 
         * header é outra objeto implicito.
         *
         * Para acessar um atributos, informe no colchetes o nome da propriedade do objeto. 
         *
         * Mesmo que (request.getHeader("host")) 
        -->
        
        Seu host é: ${header["host"]}
        
    </body>
</html>
