
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!-- 
     A diretiva taglib possui 2 atributos.
 
     @prefix é utilizado para identificar na JSP o arquivo tdl solicitado
     @uri informa ao container o nome da TDL 
-->

<%@taglib prefix="calcular" uri="/WEB-INF/tlds/calcular.tld" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Média Escolar</title>
    </head>
    <body>
        <h1>Resultado</h1>
        
        <!-- 
         * JSP são transformados em servlets de primeira classe e recebem seus próprios ServletConfig. 
         * 
         * O Objeto ServletConfig de outros Servlets podem ser passados para este por meio de parametros.
        -->
        
       <!-- 
         * Expression Languages, ou simplesmente EL.
         *
         * O EL surgiu para substituir os scriptlets que vinhamos utilizando nos exemplos anteriores 
         * dentro das paginas JSP entre as TAGs '<'%...%'>'. 
         *
         * O EL permite que possamos referenciar as Beans e acessar os metodos dos objetos. 
         *
         * Através do EL também podemos acessar o Objeto implicito 'param' para acessar os atributos de request.
        -->
        
        <!-- 
         * Os objetos implicitos são simples Maps para nome/valor 
         *
         * após o operador ponto, deve se utilizar o ID utilizado no input do formulario que fez a requisição.
        
        Nome: ${param.nome} <br> 
        Sobrenome: ${param.sobreNome} <br>
        
        <!-- 
         * Se o formulario que fez a requisição possuir IDs identicos, os valores serão inclusos em um ArrayList.
         *
         * ParamValue permite acessar os diferentes valores para o parametro.
         *
         * Considere que o formulario da pagina index.html possui dois inputs com o mesmo nome, 'nota'.
        -->
        
        Nota 1: ${paramValues.nota[0]}<br>
        Nota 2: ${paramValues.nota[1]}<br>
        
        
        
        
        <!-- 
         * Faz uma chamada ao método "media" da classe Media.java, q foi apelidado de calcularMedia na arquivo tdl
         *
         * O metodo precisa ser estatico 
         *
         * Caso a El não encontre um valor, seja ele uma chave/propriedade/atributo, a pagina ainda assim será exibida 
         * com o campo de valores em banco.
         *
         * Nas expressões aritiméticas a El trata uma variavel desconhecida como zero.
         *
         * Nas expressões lógicas, a El trata uma variavel desconhecida como false. 
        -->
        
        <%-- 
             A EL pode conter expressoes aritiméticas, logica e relacionais. 
             ${num > 3}
             ${integer le 12}
             ${requestScope[nome] and true}
        --%>
        
        <!-- A chamada deve conter o prefixo da taglib precedido do Pseudonimo declarado no arquivo tld-->
        Media: ${calcular:calcularMedia(paramValues.nota[0], paramValues.nota[1])}
        
        
        <br>
        <!-- 
         * header é outra objeto implicito.
         *
         * No caso de atributos, informe no colchetes o nome da propriedade do objeto. 
         * 
         * Mesmo que (request.getHeader("host")) 
        -->
        
        Seu host é: ${header["host"]}
        

    </body>
</html>
