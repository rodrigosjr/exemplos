<!-- 
 * Esta pagina complementa � pagina media.jsp, mas diferente da diretiva '@include' a a��o 'jsp:include' n�o ir� mover 
 * o fonte desta pagina para a pagina media.jsp.
 *
 * Ao encontrar a a��o 'jsp:include' o container ir� inserir uma chamada a um metodo no c�digo do servlet gerado, 
 * que, no rumtime, combinar� dinamicamente a resposta vinda do Servlet logo.jsp com a resposta de media.jsp. 
 *
 * Recaptulando:
 *   - A pagina media.jsp � convetida em uma classe, media_jsp.java. 
 *   - O container compila a Classe e Instancia para criar o Objeto Servlet.
 *
 * Resumidamente temos dois servlets que combinam suas respostas para retornar uma pagina para o cliente.
-->
    
<!-- 
 * No EL a variavel da esquerda precisa ser um Map ou um Bean, enquanto que o que estiver na direita precisa ser uma 
 * Chave de Map ou propriedade Bean.
--> 
<img src="IMG/matematica.jpg" >
<!-- Parametro do Objeto request no MAP param -->
<br>Seja bem vindo ${param.nome}, 
<!-- Atributo do Objeto request disponivel no MAP requestScope -->
nota para a materia de ${requestScope["materia"]}
       
