
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Média Escolar</title>
    </head>
    <body>
       
        <!-- 
         * JSP são transformados em servlets de primeira classe e recebem seus próprios ServletConfig. 
         * 
         * O Objeto ServletConfig de outros Servlets podem ser passados para este por meio de parametros.
        -->
        
       <!-- 
         * O conteudo da jsp inserida nesta pagina NÃO fará parte do servlet, como na diretiva '@include'.
         *
         * A JSP será tradudiza em uma class, e a resposta desta classe será combinada com a resposta da pagina
         * formando uma unica pagina combinada.
        -->

        <!-- 
         * Altera o parametro 'nome' da solicitação efetuada pelo formulario da pagina index.html
         * 
         * Os cabeçalhos costumam ter textos estaticos, mas em algumas situações você pode querer alterar o texto 
         * para se adequar a um contexto especifico.
        -->
        <jsp:include page="logo.jsp">
            <jsp:param name="nome" value="Caro(a). Aluno"/>
        </jsp:include>
        
        
        <% 
           /**
             * Capitura os parametros do Objeto request.
             * 
             * O objeto request e response foi recebido pelo servlet Calcular.java e posteriormente encaminhado a JSP
             * vide implementação do metodo processRequest().
             * 
             * O container vê q a solicitação do cliente é para um servlet e cria dois Objetos, HttpServletRequest e 
             * HttpServletResponse respectivamente.
             */
        
            /**
             * O container cria um par de String nome/valor e passa como parametro para os objetos request e response.
             * O valor pode ser recuperado ao informar o nome para o metodo getParameter() da Classe ServeletRequest.java
             * extendida por HttpServletRequest.java.
             */
        
            Integer n1 = Integer.parseInt(request.getParameter("nota1"));
            Integer n2 = Integer.parseInt(request.getParameter("nota2"));
            
            /**
             * O JSP ira gerar um Response HTTP.
             * O Response HTTP contem uma pagina HTML que será entregue ao bowser.
             */ 
                       
            out.println("<h1>Servlet Calcular at " + request.getContextPath () + "</h1>");
            out.println("<br> Nota 1: " + n1 + "<br> Nota 2: "+ n2);
            out.println("<br>Média: " + (n1 + n2) /2);
        %>
        
<br><br>Note que o valor do parametro do Servlet media.java não foi alterado:  ${param.nome}
    
    </body>
</html>
