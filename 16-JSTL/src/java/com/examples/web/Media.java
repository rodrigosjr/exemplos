
package com.examples.web;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "media", urlPatterns = {"/media"})
public class Media extends HttpServlet {

     /**
     * O método doPost() proveniente da classe extendida HttpServelt fará uma chamada ao metodo processRequest.
     * O metodo processRequest é uma implementação da Classe Calcular.java, o metodo poderia ter qualquer outro nome.
     * O servlet suporta chamadas HTTP do tipo Post, o metodo doPost() está delegando a referencia de request e response.
     */
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        /**
        * Define a resposta como sendo um HTML, note que response é a referencia recebida de doPost().
        */
            
        response.setContentType("text/html;charset=UTF-8");
        
        String [] descricao = {"Obrigado", "por", "utiliar", "os", "nossos", "serviços"};
        
        request.setAttribute("titulo", descricao);

        /**
         * O RequestDispatcher tem como objetivo Redirecionar o controle para um outro componente da aplicação.
         * O controle é atribuido a media.jsp para que ele termine de atender a solicitação do cliente.
         * 
         * JSP são transformados em servlets de primeira classe e recebem seus próprios ServletConfig.
         */
            
        RequestDispatcher rd = request.getRequestDispatcher("resultado.jsp");
        
        /**
         * Atribui o controle do request e response ao JSP.
         */
            
        rd.forward(request, response);
        

    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
   
    }

    /**
     * Delega a processRequest todos os privilégios de doPost.
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

 
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
