<%-- 
    Document   : index
    Created on : 21/05/2012, 10:18:31
    Author     : rodrigo.nascimento
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calcule sua média</title>
    </head>
    <body>
        Calcule sua média<br>
      <!-- 
        * A URL do ***action*** esta mapeada no Deployment Descripitor (***web.xml***).
        * Trata-se de uma pagina dinâmica que não existe fisicamente e será criada no momento em que o botão 'Resposta' for acionado.
        * A URL do action está vinculada a Classe Media.java e ***method*** indica que será feita uma requisição ao seu metodo doPost().
      -->
        <form action="calcularMedia.do" method="post">
              Nome: <input name="nome" type="text" /><br>
            Nota 1: <input name="nota1" type="text" /><br>
            Nota 2: <input name="nota2" type="text" /><br>
            <input type="submit" value="Calcular" />
        </form>
    </body>
</html>
