<%-- 
    Document   : resultado
    Created on : 18/05/2012, 14:16:02
    Author     : rodrigo.nascimento
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%--  
 * Para utilizar o jstl é preciso declarar a diretiva taglib.
 * O atributo "prefix" declara qual o prefixo do Jstl será utilizado.
 * O atributo "uri" recebe como parametro o core jstl.
 * Para utilizar o jstl, antes será preciso adicionar a biblioteca jstl 1.2.jar ao projeto.
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Resultaado</title>
    </head>
    <body>
        
        <%--
         * <c:choose> ajuda a criar multiplas condicionais seguida por um valor padrão.
         * <c:when> é utilizando para efetuar os testes condicionais.
         * <c:otherwise> é utilizado para definir o valor padrão.
         *
         * <c:when> possui um atributo "test" que pode ser utilizado em conjunto com a EL para efetuar testes condicionais.
         * A EL possui diversos operadores logicos e condicionais.
        --%>
        
        <c:choose>
            <c:when test="${param.nome eq ''}"> 
                <c:out value="Bem vindo, Sr Desconhecido." />
            </c:when>
            <c:otherwise>  
                <c:out value="Bem vindo, ${param.nome}" />
            </c:otherwise>
        </c:choose>
        
        <br>
        <br>
        
        Nota do primeiro semestre: ${param.nota1}<br>
        Nota do segundo semestre: ${param.nota2}<br>
        
        <%-- 
         * <c:set > permite-lhe criar/alterar um novo atributo no escopo da solicitação atrasvés do atributo "var".
         * <c:set > permite-lhe definir valores em um Map ou propriedades de um bean através do atributo "target".
         *
         * O atributo "var" declara um atributo de escopo, e caso este atributo não exista no escopo, um novo atributo
         * será criado.
         *
         * O atributo "target" declara um bean ou um map, o parametro de "target" não pode ser uma String, e deve ser 
         * referenciado por um Objeto (utilize EL).
         *
         * Se o "value" for nulo, o atributo nomeado por "var" será removido.
         *
         * Ou seja, informar o nome do atributo em "var" e manter "value" em branco fará com que qualquer atributo 
         * do escopo que correspondente seja apagado.
         *
         * A EL pode ser utilizada para efetuar operações matematicas.
        --%>
        
        <!-- Inclui na requisição o atributo media com a nota do aluno -->
        <c:set var="media" value="${(param.nota1 + param.nota2) / 2}" scope="request" />
        
        <!-- O operador colchetes não é aceito em uma expressão EL declarada como atributo no JSTL  -->
        
        Média anual: ${requestScope["media"]}

        <%-- <c:if > inclui uma condicional na pagina HTML. --%>
        
        <c:if test="${requestScope.media ge 5}" >
            <br><br>Parabéns, você foi aprovado.<br><br>
        </c:if>
            
        <c:if test="${requestScope.media le 5}" >
            <br><br>Desculpe, você foi reprovado.<br><br>
        </c:if>
        
        <%-- <c:forEach > integra-se perfeitamente com um loop FOR. --%>
        
        <c:forEach var="palavra" items="${requestScope.titulo}" >
            <c:out value="${palavra}" />
        </c:forEach>
    </body>
</html>
