
<%@page contentType="text/html" pageEncoding="UTF-8"  %>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Resultado</title>
    </head>
    <body>
        <h1>Confira o resultado:</h1><br><br>
        
        <!-- 
         * Aqui pode haver uma excecao, e se houver, a pagina de erro definida no DD sera chamada.
         *
         * O request e o response sera disponibilizado para a pagina de erro 
        -->
        ${param.dividendo} / ${param.divisor} = ${param.dividendo / param.divisor}

            
    </body>
</html>
