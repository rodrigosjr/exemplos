<!-- 
 * A diretiva page possui um atributo "isErrorPage" que recebe como parametro um true or false.
 * True indica que esta pagina 'e uma pagina de erro.
-->

<%@page contentType="text/html" pageEncoding="UTF-8" isErrorPage="true" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Error</title>
    </head>
    <body>
        <h1>Você informou um valor diferente do tipo numérico!</h1><br><br>
        <!-- Os atributos do request encaminhado para a pagina resultado estao tambem disponiveis nesta pagina -->
        Dividendo: ${param.dividendo}<br>
        Divisor: ${param.divisor}

    </body>
</html>
