
<%@page contentType="text/html" pageEncoding="UTF-8"  %>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Resultado</title>
    </head>
    <body>
        <h1>Confira o resultado:</h1><br><br>
        
        <!-- 
         * Aqui pode haver uma excecao, e se houver, a pagina de erro definida no DD NÃO sera chamada. 
         *
         * A exceção sera capturada por '<'c:catch'>', e por este motivo não visualizaremos a pagina de erro definida no DD.
         *
         * Cuidado com o catch, pois, mesmo que o texto esteja dentro do corpo de '<'c:catch'>', todo o texto 
         * que vier ANTES do codigo que causa a exceção será exibido.
         *
         * Todo o texto que vier depois do código que causa a exceção NÃO será exibido, desde que esteja entre de
         * '<'c:catch'>'.
         *
         * Por este motivo utilizamos '<'c:out'>' para contornar este fato.
        -->
        <c:catch var="exception" >
            <c:out value="${param.dividendo} / ${param.divisor} = ${param.dividendo / param.divisor}" />
        </c:catch>
        

        <!-- 
         * A variavel exception declarada em '<'c:catch'>' é um MAP contendo os parametros do objeto exception 
         * A variavel exception deve ser utilizada fora do corpo e depois da declaração de '<'c:catch'>' 
        -->
        <c:if test="${exception ne null}" >
             Desculpe, ocorreu um erro na divisão.<br>
             Motivo: <b>${exception}</b> 
        </c:if>

            
    </body>
</html>
