
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!-- A uri pode ser um caminho totalmente qualificado ou um peseudonio definido na tag <uri> do arquivo TLD -->
<%@taglib prefix="m" uri="tag-customizadas" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calcule sua média</title>
    </head>
    <body>
        <h1>Resultado da média anual</h1><br>
        
        <m:aluno nome="${param.nome}" nota1="${param.nota1}"  nota2="${param.nota2}"   />  
        
        ${param.dividendo} / ${param.divisor} = ${param.dividendo / param.divisor}

    </body>
</html>
