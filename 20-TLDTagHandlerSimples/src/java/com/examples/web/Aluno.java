package com.examples.web;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 *
 * @author rodrigo.nascimento
 */

/**
 * Para instanciar a classe a partir do TLD é preciso extender SimpleTagSupport
 */
public class Aluno extends SimpleTagSupport {
    private String nome;
    private int nota1;
    private int nota2;


    public String getNome() {
        return nome;
    }
    //É preciso criar um metodo setter para cada variavel de instancia utilizada como atributo no TLD.
    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getNota1() {
        return nota1;
    }

    public void setNota1(int nota1) {
        this.nota1 = nota1;
    }

    public int getNota2() {
        return nota2;
    }

    public void setNota2(int nota2) {
        this.nota2 = nota2;
    }
}
