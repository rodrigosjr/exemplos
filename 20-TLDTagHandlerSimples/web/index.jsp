<%-- 
    Document   : index
    Created on : 23/05/2012, 14:40:14
    Author     : rodrigo.nascimento
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Auxiliar de divisão</title>
    </head>
    <body>
        <h1>Entre com os valores:</h1><br><br>
      <!-- 
        * A URL do ***action*** esta mapeada no Deployment Descripitor (***web.xml***).
        * Diferente dos exemplos anteriores, neste caso a pagina em JSP existe e o usuário será direcionado ao clicar no botão 'Dividir'.
        * Neste exemplo o action irá direcionar a solicitação diretamente para a pagina resultado.jsp e ***method*** indica que será feita uma chamada do tipo post.
      -->
        <form action="resultado.jsp" method="get" >
                Nome: <input name="nome" type="text" /><br>
                Dividendo: <input name="dividendo" type="text" /><br>
                Divisor: <input name="divisor" type="text" /><br>
                <input type="submit" value="Didivir" />
        </form>
    </body>
</html>
