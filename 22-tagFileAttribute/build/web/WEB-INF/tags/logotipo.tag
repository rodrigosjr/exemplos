<%@tag pageEncoding="UTF-8"%>

<!-- A tag "attribute" declara um atributo com o nome "legends", do tipo String, e com declaração obrigatoria -->
<%@attribute name="legends" required="true" type="java.lang.String" %>

<img src="IMAGENS/ChibiNaruto.jpg" /> <br>

<!-- Dentro da tag é possivel efetuar chamadas ao atributo a qualquer momento utilizando a EL -->
${legends}
