<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!-- Importa todas as tags do diretorio TAGs e declara um prefixo para referencia-las -->
<%@taglib prefix="customTags" tagdir="/WEB-INF/tags/"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <!-- 
         * Importa a tag logotipo, do diretorio tags, por meio do prefixo customTag
         * 
         * A tag "customTags" declara o atributo "legends".
         *
         * A declaração do atributo é obrigatória, pois assim foi definido na PAGINA-TAG.
         *
         * O atributo legends pode ser utilizado com a ajuda da EL, porem dentro do contesto da tag, ou seja,
         * dentro do corpo da tag.
        -->
        <customTags:logotipo legends="Chibi Naruto" />
    </body>
</html>
