<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!-- Importa todas as tags do diretorio TAGs e declara um prefixo para referencia-las -->
<%@taglib prefix="customTags" tagdir="/WEB-INF/tags/"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <!-- 
         * Qualquer valor que esteja dentro do corpo da tag "customTags" será passado como parametro para PAGE-TAG.
         * O conteudo dentro da Tag não será exibido nesta pagina.
         * 
         * Você só verá o valor na pagina porque a ação-padrão '<'jsp:doBody/'>' irá exibir o conteudo dentro da PAGE-TAG.
        -->

        <customTags:logotipo >
            Chibi Naruto!
        </customTags:logotipo>

            
    </body>
</html>
