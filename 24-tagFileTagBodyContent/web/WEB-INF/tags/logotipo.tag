<!-- 
 * A diretiva tag permite dentre outras coisas, declarar o tipo body-content 
 *
 * Para uma tag customizada, o elemento <body-content> dentro do elemento <tag> de uma TLD é obrigatório.
 * 
 * A tag file não precisa declarar <body-content> se o padrão "scriptless" for aceitavel.
 *
 * - SCRIPTLESS permite que scriptless, EL e Tags seja utilizado no corpo da Tag File.
 * - TAGDEPENDENT faz com que o corpor de Tag File trate todo o conteudo como texto; Scriptless, EL e Tag não serão avaliados.
 * - EMPTY informa que a Tag File não pode ter um corpo.
 *
 * Uma exception ocorrerá se um corpo for declarado quando "body-content" tiver o valor empty.
-->
<%@tag body-content="tagdependent" pageEncoding="UTF-8"%>

<img src="IMAGENS/ChibiNaruto.jpg" /> <br>
<!-- A ação-padrão "doBody" exibe onde for declarada, todo o valor do corpo da tag usada para invocar esta PAGE-TAG -->
<jsp:doBody/>
    

