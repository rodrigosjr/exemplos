<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!-- A uri pode ser um caminho totalmente qualificado ou um peseudonio definido na tag <uri> do arquivo TLD -->
<%@taglib prefix="m" uri="tag-customizadas" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calcule sua média</title>
    </head>
    <body>
        <h1>Resultado da média anual</h1><br>
        
        <!-- A tag aluno esta sendo invocada com um corpo, conforme o especificado em <body-content> na tag TLD -->
        <m:aluno nome="${param.nome}" nota1="${param.nota1}" nota2="${param.nota2}" >
              <!-- O corpo ajuda a definir o que irá ou não ser apresentado na tela em caso de exception -->
              Sua média é: ${(param.nota1 + param.nota2)/2}
        </m:aluno>
    </body>
</html>
