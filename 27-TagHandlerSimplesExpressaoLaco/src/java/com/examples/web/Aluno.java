/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.examples.web;
import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 *
 * @author rodrigo.nascimento
 */

/**
 * Para instanciar a classe a partir do TLD é preciso extender SimpleTagSupport
 */
public class Aluno extends SimpleTagSupport {
    private String nome;
    private int nota1;
    private int nota2;

    @Override
    public void doTag() throws IOException, JspException{

        /**
         * processa o corpo da tag e exibe-o na resposta.
         * O argumento nulo significa que output vai para a resposta, em vez de algum outro write que você informe.
         */

        /*
         * Se fosse utilizado um for para percorrer uma lista que seria inserida no corpo da tag handler
         * o metodo getJspBody teria que estar dentro do corpo do FOR para inserir cada um dos valores do array de forma 
         * individual
         */
        
        String[] nomes = {"Rodrigo", "João", "Maria"};
        
        for (String nome:nomes){
           getJspContext().setAttribute("nomes", nome); 
           getJspBody().invoke(null);
        }
        
    }

    public String getNome() {
        return nome;
    }
    //É preciso criar um metodo setter para cada variavel de instancia utilizada como atributo no TLD.
    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getNota1() {
        return nota1;
    }

    public void setNota1(int nota1) {
        this.nota1 = nota1;
    }

    public int getNota2() {
        return nota2;
    }

    public void setNota2(int nota2) {
        this.nota2 = nota2;
    }
}
